# Workstation

## Purpose

This repostory houses command-line utilities for installing commonly used software and configurations on a workstation.
This can be used by Black Label team members or customers to get software (like the following) installed:

- DoD Certificate Authority (CA) Certificates
  - In the operating system CA trust
  - In the Chrome/Chromium browser trust
  - In the Firefox browser trust
- Common Access Card (CAC) Drivers
- AppGate SDP Client
- VMWare Horizon Client (for Desktop Anywhere usage)

This repository is intended to host setup instructions and code for multiple operating systems,
and contributing code that works on additional ones is always welcome, assuming you follow the contribution guidelines.

## Support Matrix

Below is a table containing a list of operating system (OS) families, and the features supported for each one.
This list is not granular. There may be specific distribution/version combinations that are NOT supported,
even if the feature is listed as supported for the OS family that the particular distribution/version combination belongs to.

Support levels vary for features as well. See key below table for testing levels and types.

| **Support Matrix**     | Linux  | ChromeOS  | MacOS  | Windows  | BSD  |
|------------------------|--------|-----------|--------|----------|------|
| CAC Drivers            |   X    |     X     |   X    |    X     |  X   |
| DoD CA Certs (System)  |   X    |     X     |   X    |    X     |  X   |
| DoD CA Certs (Firefox) |   X    |     X     |   X    |    X     |  X   |
| DoD CA Certs (Chrome)  |   X    |     X     |   X    |    X     |  X   |
| AppGate SDP Client     |   X    |     X     |   X    |    X     |  X   |
| VMWare Horizon Client  |   X    |     X     |   X    |    X     |  X   |

**Key**
**P = Tested (via remote pipeline env)**
**L = Tested (via local test env)**
**U = Untested**
**X = Not Supported for this OS family**

## Installation

Installation varies by operating system family. Please follow the instructions in `docs/` specific to your operating system family.

## Contributing

Contribution instructions are located in the `CONTRIBUTING.MD` and vary by installation script and operating system family.

## Reporting Issues

Please use the Gitlab issues feature to report issues with the code in this repository. We have some issue templates available to assist you when creating issues.

## Archive

The `archive/` directory in this repository contains some legacy code that we are working on migrating over into the appropriate place (or removing). This code is untested and unsupported.
