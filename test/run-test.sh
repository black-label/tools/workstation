#!/usr/bin/env bash

# @name run-test.sh
# @brief Runs a test in a GitlabCI pipeline
# @description
#     This script emulates the run-test subcommand in ./dev/dev-env.sh,
#     but in a GitlabCI pipeline instead of locally.

# @description
#     Main (and only) function.
#     Accepts an expected rc and script name plus args to run.
#     Runs the script, checks the rc,
#     and then either passes or fails based on whether the expected rc matches the actual rc.
main() {
  declare expected_rc
  declare actual_rc
  declare -a script
  expected_rc="${1}"
  shift
  script=( "${@}" )
  # shellcheck disable=SC2086
  /usr/bin/env bash "${script[@]}"
  actual_rc="${?}"
  if [[ "$actual_rc" != "$expected_rc" ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Actual return code (${actual_rc}) did not match expected return code (${expected_rc})." 1>&2
    return 1
  else
    echo -e "\n[PASS] ${FUNCNAME[0]} | Actual return code (${actual_rc}) matched expected return code (${expected_rc}) for script (${script[0]})."
  fi
}

# @internal
#     Calls the main function and captures the return code,
#     using that to determine the overall exit code.
main "$@" || { rc="${?}"; exit "$rc"; }

