## Summary

<!--Summarize the bug encountered concisely-->

## Steps to reproduce

<details>
<summary>Describe how one can reproduce the issue - this is very important. Please use an ordered list.</summary>
<pre>
- [ ] Step 1 - I did this
- [ ] Step 2 - Then this
- [ ] Unexpected Result - Then this happened see this attached screenshot!
</pre>
</details>

## What is the current *bug* behavior?

<!-- Describe what actually happens. -->

## What is the expected *correct* behavior?

<!-- Describe what you should see instead. -->

## Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->


## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

## Priority/Severity
<!-- Delete as appropriate. The priority and severity assigned may be different to this !-->
~priority::high (anything that impacts the normal user flow or blocks app usage)  
~priority::medium (anything that negatively affects the user experience)  
~priority::low (anything else e.g., typos, missing icons, layout issues, etc.)  
~"BUG" 
