#!/usr/bin/env bash

# @name workstation.sh
# @brief A workstation setup command-line utility
# @description
#    This file can be executed to perform common workstation setup tasks.
#    The target audience is Black Label service providers and service consumers.

# @description
#     This function prints a usage message
wsUsage() {
  echo ""
  echo "Usage: ${BASH_SOURCE[0]##*/} [options] <subcommand>"
  echo ""
  echo "  Options:"
  echo "    --verbose|-v|-V: prints debug messaging"
  echo "    --debug|-d|-D: prints debug messaging"
  echo ""
  echo "  Subcommands:"
  echo "    help: prints this usage message (default)"
  echo ""
}

# @description
#     This function prints info messages in a standardized way.
#     It should be called by all other functions passing messages to stdout/stderr.
wsInfo() {
  local caller
  local message
  # Default to shell if caller not another function
  caller="${FUNCNAME[1]:-shell}"
  if [[ -n "$1" ]]; then
    message="${1}"
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Failed to pass a message to print." 1>&2
    return 1
  fi
  echo -e "\n[INFO] ${caller} | ${message}"
  return 0
}

# @description
#     This function prints debug messages in a standardized way.
#     It should be called by all other functions passing messages to stdout/stderr.
wsDebug() {
  local caller
  local message
  local debug
  debug="${_ec_ws_debug}"
  if [[ "${debug}" != "on" ]]; then
    return 0
  fi
  # Default to shell if caller not another function
  caller="${FUNCNAME[1]:-shell}"
  if [[ -n "$1" ]]; then
    message="${1}"
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Failed to pass a message to print." 1>&2
    return 1
  fi
  echo -e "\n[DEBUG] ${caller} | ${message}"
  return 0
}

# @description
#     This function prints failure messages in a standardized way.
#     It should be called by all other functions passing messages to stdout/stderr.
wsFail() {
  local caller
  local message
  # Default to shell if caller not another function
  caller="${FUNCNAME[1]:-shell}"
  if [[ -n "$1" ]]; then
    message="${1}"
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Failed to pass a message to print." 1>&2
    return 1
  fi
  echo -e "\n[FAIL] ${caller} | ${message}" 1>&2
  return 1
}

# @description
#     This function prints warning messages in a standardized way.
#     It should be called by all other functions passing messages to stdout/stderr.
wsWarn() {
  local caller
  local message
  # Default to shell if caller not another function
  caller="${FUNCNAME[1]:-shell}"
  if [[ -n "$1" ]]; then
    message="${1}"
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Failed to pass a message to print." 1>&2
    return 1
  fi
  echo -e "\n[WARN] ${caller} | ${message}" 1>&2
  return 0
}

# @description
#     This function determines the directory that the script itself resides in.
#     This directory path is then used as a relative reference point by other functions.
#     This is helpful if the script is called from somewhere other than the directory it resides in.
#     @noargs
wsGetScriptDir() {
  # Ensures pwd and dirname bins exist before trying to use them to parse and cd to script path
  # Test fails if which is passed a bash builtin (i.e. cd), or returns multiple paths, or which not in path
  if [[ -f "$(which pwd)" ]] && [[ -f "$(which dirname)" ]]; then
    # No _?_* prefix naming convention indicates this is a locally scoped variable
    local ws_script_dir
    ws_script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  else
    wsFail "Required dependencies (pwd and/or dirname) not present in path." || return 2
  fi
  if [[ -d "${ws_script_dir}" ]]; then
    # Exporting var to make available to subshells
    # Setting readonly/constant since var won't change after this
    # _ec_* naming convention indicates exported and readonly/constant variable types
    _ec_ws_script_dir="$ws_script_dir"
    readonly _ec_ws_script_dir
    export _ec_ws_script_dir
    wsDebug "${BASH_SOURCE[0]##*/} is located in the following directory:\n        $_ec_ws_script_dir"
  else
    wsFail "Parsed script directory path not a valid directory." || return 3
  fi
}

# @description
#    This function detects the distribution type, and saves that to a variable for use by other funtions.
#    This variable contains a string with a space delimited list of like distros.
#    This function also detects the distribution version, and saves that to a variable for use by other functions.
#    This variable contains a string with a space delimited list of versions string.
#    These variables can be used in case statements in other functions to do distro/version-specific things.
wsGetDistroInfo() {
  local distro_ids
  local distro_versions
  for file in /etc/*release; do
    if [[ ! -f "${file}" ]]; then
      wsWarn "Release file (${file}) is not a regular file; skipping."
      break
    fi
    while read -r line; do
      # If line in /etc/*release file starts with "ID=" followed by at least one character, the regex matches
      if [[ "$line" =~ ^ID=.+$ ]]; then
        local "$line"
        distro_ids+=" ${ID}"
      fi
      # If line in /etc/*release file starts with "ID_LIKE=" followed by at least one character, the regex matches
      if [[ "$line" =~ ^ID_LIKE=.+$ ]]; then
        local "$line"
        distro_ids+=" ${ID_LIKE}"
      fi
      # If line in /etc/*release file starts with "VERSION_ID=" followed by at least one character, the regex matches
      if [[ "$line" =~ ^VERSION_ID=.+$ ]]; then
        local "$line"
        distro_versions+=" ${VERSION_ID}"
      fi
    done < "${file}"
  done
  if [[ "${#distro_ids}" != "0" ]]; then
    wsDebug "Detected the following distro (and like distros): ${distro_ids}"
    _ec_ws_distro_ids="${distro_ids}"
    readonly _ec_ws_distro_ids
    export _ec_ws_distro_ids
  else
    wsFail "Failed to detect distro (and like distros)." || return 4
  fi
  if [[ "${#distro_versions}" != "0" ]]; then
    wsDebug "Detected the following distro version string(s): ${distro_versions}"
    _ec_ws_distro_versions="${distro_versions}"
    readonly _ec_ws_distro_versions
    export _ec_ws_distro_versions
  else
    # Some rolling releases don't have distro version strings
    wsWarn "Failed to detect distro version, might be a rolling release."
  fi
}

# @description
#    This function checks if the DoD Cyber Exchange provided certificates are installed.
wsCheckDodUrl() {
  wget -S --spider --timeout 11 https://afrcdesktops.us.af.mil || return 5
}

# @description
#    This function downloads the DoD Cyber Exchange certificate bundle.
wsGetDodCertBundle() {
  local cert_bundle_url
  while read -r line; do
    # Regex match any line that contains http tag that contains dod.zip in any case
    if [[ "$line" =~ http.*[Dd][Oo][Dd]\.zip ]]; then
      cert_bundle_url="${BASH_REMATCH[0]}"
      break
    fi
  done < <(curl -s 'https://public.cyber.mil/pki-pke/pkipke-document-library/')
  if [[ "${#cert_bundle_url}" == "0" ]]; then
    wsFail "Failed to get valid cert bundle"
    return 6
  fi
  wget -O /tmp/dod.zip "$cert_bundle_url" ||
    {
      wsFail "Unable to download the cert bundle ${cert_bundle_url} to /tmp"
      return 7
    }
  unzip -o -j /tmp/dod.zip -d /tmp/dod ||
    {
      wsFail "Unable to decompress cert bundle in /tmp"
      return 8
    }
  mkdir -p /tmp/certs ||
    {
      wsFail "Failed to make cert sub-directory in /tmp"
      return 9
    }
}

# @description
#    This function converts any pem files into plaintext, human-readable cert files
wsConvertPems() {
  wsDebug "Converting DoD certificates to plaintext format."
  for p7b_file in /tmp/dod/*pem.p7b; do
    pem_file="${p7b_file//.p7b/}"
    wsDebug "Converting ${p7b_file} to ${pem_file}"
    openssl \
      pkcs7 \
      -in "${p7b_file}" \
      -print_certs \
      -out "${pem_file}" ||
      {
        wsFail "Failed to convert ${p7b_file} to ${pem_file}"
        return 10
      }
  done
}

# @description
#    This function converts any der files into plaintext, human-readable cert files
wsConvertDers() {
  for p7b_file in /tmp/dod/*der.p7b; do
    der_file="${p7b_file//.p7b/}"
    wsDebug "Converting ${p7b_file} to ${der_file}"
    openssl \
      pkcs7 \
      -in "${p7b_file}" \
      -inform DER \
      -print_certs \
      -out "${der_file}" ||
      {
        wsFail "Failed to convert ${p7b_file} to ${der_file}"
        return 11
      }
  done
}

# @description
#     This function seperates multi-cert CA bundles into single-cert CA files ending in .crt
#     This is needed because some OSes only accept CA certs into their trust when their is one-per file and it ends in .crt
wsSplitCerts() {
  local bundle
  local -a individual_certs
  local -a cert_lines
  bundle="$1"
  if [[ ! -f "${bundle}" ]]; then
    wsFail "Bundle file ($bundle) not a regular file."
    return 12
  fi
  wsDebug "Splitting CA bundle file (${bundle}) into individual cert files and staging for inclusion in system CA trust."
  while read -r line; do
    # This checks a mutli-cert bundle file for the start of a valid CA cert
    # If it finds one, create a file that matches the cert name
    # Add that line to an array to write the cert to the file later
    if [[ "${line}" =~ ^[[:space:]]*subject=.* ]]; then
      individual_certs+=("${BASH_REMATCH[0]//*CN = /}")
      # Create a file using the value of the last item in the array (which should be a cert name)
      : > "/tmp/certs/${individual_certs[-1]}.crt" || return 13
      cert_lines+=("${line}")
    # If a line in the multi-cert bundle is blank, skip it
    # We don't need it included in any cert files
    elif [[ "${line}" =~ ^[[:space:]]*$ ]]; then
      continue
    # If a line in the multi-cert bundle is not blank or the cert end-line,
    # append the contents of that line to an array for use later
    elif [[ ! "${line}" =~ END.*CERTFICATE ]]; then
      cert_lines+=("${line}")
    # If a line in the multi-cert bundle indicates the end of a single cert,
    # append it to an array, and then write each element in the array to a
    # single-cert file matching the cert name from the first line of the cert
    elif [[ "${line}" =~ END.*CERTIFICATE ]]; then
      cert_lines+=("${line}")
      for cert_line in "${cert_lines[@]}"; do
        echo "${cert_line}" >> "/tmp/certs/${individual_certs[-1]}.crt" || return 14
      done
      cert_lines=()
    fi
  done < "${bundle}"
}

# @description
#    This function checks if debug/verbose mode is desired by the user.
wsIsDebugOn() {
  # Before processing the subcommand argument, check if user wants debug mode toggled on
  case "$1" in
  --[Dd]ebug | -[Dd] | --[Vv]erbose | -[Vv])
    _ec_ws_debug="on" &&
      readonly _ec_ws_debug &&
      export _ec_ws_debug &&
      return 0
    ;;
  esac
  # This return 1 only gets called if it fails to setup _ec_ws_debug properly
  return 1
}

# @description
#     This is the main function and entrypoint for the script.
#     This function contains almost no logic.
#     It just calls other functions and passes along their return codes if they fail.
wsMain() {
  declare _ec_ws_debug || return 1
  declare _ec_ws_script_dir || return 1
  declare _ec_ws_distro_ids || return 1
  declare _ec_ws_distro_versions || return 1
  # Ensure user passed a subcommand argument
  if [[ "${#}" -lt 1 ]]; then
    wsFail "Failed to passed a subcommand to the program; view usage message below."
    wsUsage
    return 1
  fi
  # If debug option is passed, shift so we move our subcommand name to $1
  wsIsDebugOn "${1}" && shift
  wsDebug "Debug mode on."
  # Parse subcommand name
  case "$1" in
  [Hh]elp)
    wsUsage
    return 0
    ;;
  [Uu]pdate-system-ca-trust)
    {
      wsGetScriptDir &&
        wsGetDistroInfo &&
        wsCheckDodUrl ||
        {
          wsGetDodCertBundle &&
            wsConvertPems &&
            wsConvertDers &&
            for bundle in /tmp/dod/*{pem,der}; do
              wsSplitCerts "${bundle}" ||
                {
                  rc="${?}"
                  return "${rc}"
                }
            done
        }
    } || {
      rc="${?}"
      return "${rc}"
    }
    ;;
  *)
    # If user passes a subcommand argument, but it doesn't match any known subcommand names, err and return
    wsFail "Invalid subcommand name (${1}) passed to program; view usage message below."
    wsUsage
    return 1
    ;;
  esac
}

# @internal
#     This is where the main function is called.
#     The return codes of functions called in main are passed up to this function call.
#     The script only exits from this location, everywhere else, it returns.
wsMain "${@}" || {
  rc="${?}"
  exit "${rc}"
}
