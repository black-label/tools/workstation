#!/usr/bin/env bash

# This script is used to ensure the test suite is working
# See the "control" test case to see how its called if you're curious
if [[ "${#BASH_VERSION}" != "0" ]]; then
  echo -e "\n[INFO] ${BASH_SOURCE[0]} | Bash Version: $BASH_VERSION"
  exit 3
fi
