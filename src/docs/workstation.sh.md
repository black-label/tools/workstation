# workstation.sh

A workstation setup command-line utility

## Overview

This file can be executed to perform common workstation setup tasks.
The target audience is Black Label service providers and service consumers.

## Index

* [wsUsage](#wsusage)
* [wsInfo](#wsinfo)
* [wsDebug](#wsdebug)
* [wsFail](#wsfail)
* [wsWarn](#wswarn)
* [wsGetScriptDir](#wsgetscriptdir)
* [wsGetDistroInfo](#wsgetdistroinfo)
* [wsCheckDodUrl](#wscheckdodurl)
* [wsGetDodCertBundle](#wsgetdodcertbundle)
* [wsConvertPems](#wsconvertpems)
* [wsConvertDers](#wsconvertders)
* [wsSplitCerts](#wssplitcerts)
* [wsIsDebugOn](#wsisdebugon)
* [wsMain](#wsmain)

### wsUsage

This function prints a usage message

### wsInfo

This function prints info messages in a standardized way.
It should be called by all other functions passing messages to stdout/stderr.

### wsDebug

This function prints debug messages in a standardized way.
It should be called by all other functions passing messages to stdout/stderr.

### wsFail

This function prints failure messages in a standardized way.
It should be called by all other functions passing messages to stdout/stderr.

### wsWarn

This function prints warning messages in a standardized way.
It should be called by all other functions passing messages to stdout/stderr.

### wsGetScriptDir

This function determines the directory that the script itself resides in.
This directory path is then used as a relative reference point by other functions.
This is helpful if the script is called from somewhere other than the directory it resides in.
@noargs

### wsGetDistroInfo

This function detects the distribution type, and saves that to a variable for use by other funtions.
This variable contains a string with a space delimited list of like distros.
This function also detects the distribution version, and saves that to a variable for use by other functions.
This variable contains a string with a space delimited list of versions string.
These variables can be used in case statements in other functions to do distro/version-specific things.

### wsCheckDodUrl

This function checks if the DoD Cyber Exchange provided certificates are installed.

### wsGetDodCertBundle

This function downloads the DoD Cyber Exchange certificate bundle.

### wsConvertPems

This function converts any pem files into plaintext, human-readable cert files

### wsConvertDers

This function converts any der files into plaintext, human-readable cert files

### wsSplitCerts

This function seperates multi-cert CA bundles into single-cert CA files ending in .crt
This is needed because some OSes only accept CA certs into their trust when their is one-per file and it ends in .crt

### wsIsDebugOn

This function checks if debug/verbose mode is desired by the user.

### wsMain

This is the main function and entrypoint for the script.
This function contains almost no logic.
It just calls other functions and passes along their return codes if they fail.

