# CONTRIBUTING

## Development and Test Environment

The `./dev/` directory includes a script called `./dev-env.sh` with several subcommands that facilitate development and testing.

- `build` will build a docker container image from `workstation.sh.Dockerfile` using the `docker-compose.yml` file, both in `./dev`
- `launch-shell` launches an interactive docker container with certain dev utils pre-installed (i.e. `shellcheck`, `shfmt`, and `shdoc`)
- `launch-entrypoint` launches a non-interactive container and executes linting, autoformating, and a automatic docs generation utils against `./src/workstation.sh`
- `run-test <test_case>` launches a single test case located in `./test/cases/`
- `run-all-tests` launches all tests located in `./test/cases/`

# Test Framework

The test framework that is called with `run-test` and `run-all-tests` uses a set of files located in each test case's directory structure to launch a container image (or Vagrantfile), and execute a set of commands located on the first line of a `before_script` file followed by a scriptname and args list located on the first line of a `script` file. The results of the script are then checked against an expected return code stored on the first line of a file called `rc` located alongside the `before_script` and `script` files. The name of the docker image to call should be located in a `container` file.

To see a working example, take a look at the example "control" test case under `./test/cases/` (or any other test case in that same directory).

Here is what a new test cases directory with a case called "control" should look like:

```
cases
└── control
    ├── before_script
    ├── container
    ├── rc
    └── script
```

The contents of those files:

```
$ for file in test/cases/control/*; do echo -e "$file:  $(<$file)"; done | column -t -l2
test/cases/control/before_script:   apk add bash
test/cases/control/container:       alpine:latest
test/cases/control/rc:              3
test/cases/control/script:          ./src/control.sh test
```

If the `./src/control.sh test` script exits 3, the test case passes; otherwise, it fails.

After a successfully run local test, part of the `run-test` and `run-all-tests` subcommands is a code block that automatically generates a Dockerfile (to help speed up local testing) and a `.gitlab-ci.yml` for each test case that will emulate the same local test that was just performed, but in a GitlabCI pipeline. The main `.gitlab-ci.yml` for this repository uses a wildcard `include:` statement to include all of these automatically generated test jobs into its pipeline in the "test" stage. This ensures you should get nearly identical results when running tests locally (with ./dev-env.sh run-all-tests) and remotely in a MR pipeline. The `.gitlab-ci.yml` for the example `control` test case above looks like this:

```
---
control:
  stage: test
  image: alpine:latest
  before_script:
    - apk add bash
  script:
    - ./test/run-test.sh 3 ./src/control.sh test
  only:
    - merge_requests
...
```

## Standards

### Programming Languages

The code used to install software on workstations should be stored in `./src/`. Code for unix-like systems will be written in the latest Bash and code for Windows in the latest version of Powershell. If a workstation does not have the latest version of either, include instructions for how to install it in the README for that OS family, or specific distro/version combos.

### Dependency Management

External dependencies should be avoided whenever possible. This means a preference for features native to the programming language, or single-compiled binaries easy to find and install on the system through native package managers. If you do require external dependencies, keep in mind that some common utilities might have different flags on different operating systems, and using them might require if-statement logic to run them differently depending on the operating system. If you do require a dependency, do a check for it in the code at the start of the function that requires it. If that dependency functions differently on different OSes, or ships at different versions on different OSes with significantly different flags, options, or subcommands, consider doing a version check as well.

### Modularity

Code should be in functions, variables should be scoped to a local context whenever possible or reasonable. Functions should avoid calling other functions when possible, and main/entrypoint functions should be concise and outsource the majority of the code bases logic to other functions.

### Conventions

Follow the existing conventions in the file you are working on for the language you are writing in. If functions are camelcased and have a namespace prefix, while non-local variables use underscores and lowercased characters, follow that same convention in new functions.

### Documentation

Document ALL regex and string manipulations. Document anything you had to look up on StackOverflow/Google (include a link!). Do NOT walk other developers through the HOW of a function, but DO walk them through the WHY of the function. If the language and file you are working in has comments laid out in a specific way to allow for an autodocs tool to generate documentation, follow that same convention. When adding new features, update the main `README.MD` and usage messages in the utility itself.

### Quality Assurance

Write tests for your code! Check return codes against expected ones using the testing framework. Do error handling. Do input validation. Follow the advice of a linter (i.e. `shellcheck` for Bash) and run an autoformatter (i.e. `shfmt` for Bash). Before approving a new MR, make sure any new functions have at least two tests: one that test a passing case, and one that tests a failing one.
