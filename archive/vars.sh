#!/bin/bash

export DEFAULT_DOD_CERT_BUNDLE=https://dl.dod.cyber.mil/wp-content/uploads/pki-pke/zip/unclass-certificates_pkcs7_v5-6_dod.zip

find_cert_bundle() {
    url='https://public.cyber.mil/pki-pke/pkipke-document-library/'
    curl -s "$url" | awk -F '"' 'tolower($2) ~ /dod.zip/ {print $2}' | grep -i dod.zip
}

DOD_CERT_BUNDLE="$(find_cert_bundle || echo -n "$DEFAULT_DOD_CERT_BUNDLE")"

export DOD_CERT_BUNDLE
