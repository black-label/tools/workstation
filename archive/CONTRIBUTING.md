# CONTRIBUTING

## Installer

### Development and Testing Environment Choice

It is recommended that developers utilize one (or more) of the recommended development environments prior to submitting merge requests.

Current environments include:

- Docker
- Vagrant

Read below for more details on which environment is suitable for which stages of development and testing.

### Development with Docker Environment

This repository includes a built-in docker-based development environment, if you would like to use it.

To build it, just run the following `docker build` command from within your local `git clone` directory of the repo:

```
docker build -t da-installer-dev .
```

To activate it, just make sure the required directory exists, and run the following `docker run` command:

```
sudo mkdir -p /var/run/pcscd/ && \
docker run \
  --rm \
  -it \
  --privileged \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v /etc/localtime:/etc/localtime:ro \
  -v ${PWD}:/workdir \
  -v /var/run/pcscd/:/var/run/pcscd/ \
  --device /dev/bus/usb/:/dev/bus/usb/ \
  --network host \
  -e DISPLAY=$DISPLAY \
  --entrypoint /bin/bash \
  da-installer-dev;
```

This development environment includes tools like `shellcheck`, `vim`, and other utilities for working on the Desktop Anywhere installer scripts.
The `entrypoint.sh` script can be used as the entry point for automated testing within CI/CD pipelines, but is also designed with local development in mind.

The operations performed by the test script (`entrypoint.sh`) are currently divided into three stages/steps:

- Pre-installation (i.e. linting using `shellcheck`)
- Installation (i.e. the install of `vmware-view`, authentication-related tools, and deps)
- Post-Installation (i.e. steps required to launch the `vmware-view`, `pcscd`, etc.)

After entering the docker container using the `docker run` command with a `/bin/bash` entrypoint, the developer should `chmod u+x entrypoint.sh`. This script provides the following features:

- `./entrypoint.sh --reader` will tell the test script to assume a smart card reader is attached to your PC when testing the installer scripts
- `./entrypoint.sh --no-reader` will tell the test script to assume no smart card reader is attached;
  this is helpful when testing on systems without a reader attached. This is also a default option if no reader-related options are passed
- `./entrypoint.sh --no-install` will tell the test script not to run/rerun the installer script;
  this is useful for testing only the post-install steps that run in `entrypoint.sh`
- `./entrypoint.sh --install` will tell the test script to run/rerun the installation;
  this is the default option if no install-related flags are passed
- `./entrypoint.sh --lint-only` will simply lint the `entrypoint.sh` script and the installer scripts, and then exit;
  if not used, linting will run inline, prior to the install and post-install steps

Once you have completed a development iteration, please exit the container and run a clean test using the testing methodology listed below.

Note: Prior to running `entrypoint.sh` in the docker environment, ensure that `pcscd.service` and `pcscd.socket` are stopped on the host machine. 

If not, the instance of `pcscd` will not be able to create a socket, and you will not be able to access the cac reader from inside the container.
If you do not care about this because you are not testing the abilities of the cac reader, do not pass the flag to `entrypoint.sh` that trigger it to start `pcscd`.

### Testing with Docker Environment

Once you are ready to commit and submit a merge request, please perform a clean test like so:

```
docker build -t da-installer-dev .
```

Once built, validate functionality with this `docker run` command:

```
sudo mkdir -p /var/run/pcscd/ && \
docker run \
  --rm \
  -it \
  --privileged \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v /etc/localtime:/etc/localtime:ro \
  -v ${PWD}:/workdir \
  -v /var/run/pcscd/:/var/run/pcscd/ \
  --device /dev/bus/usb/:/dev/bus/usb/ \
  --network host \
  -e DISPLAY=$DISPLAY \
  da-installer-dev;
```

This final `docker run` test is similar to the development command above, with one important exception:
the entrypoint is not overwritten, and the `entrypoint.sh` script is called automattically without the need to gain shell access to the docker container.

### Test coverage Provided by Docker Environment

The test defined in the `entrypoint.sh` test script or inline in the installer script(s) that are executed during the testing `docker run` command check the following:

- Does `psccd` start?
- Does `vmware-view` start?
- If a reader is attached, can `vmware-view` access it?
- Were the DoD certs successfully downloaded and installed from the DISA's website?
- Is `afrcdesktops.us.af.mil` reachable after the certs are installed?

Things that are not tested currently:

- No shell unit tests are performed with bats, shunit2, et cetera
- Is `vmware-view` able to use `pcscd` to access and retrieve the cac certificates? This must be verified by the user manually.
- Is the cac valid, and when a pin is entered, does it successfully authenticate to `afrcdesktops.us.af.mil`? This must be verified by the user manually.
- If successful authentication is performed, the performance of applications that the user then has access to is not guaranteed;
  there are likely several libraries missing that prevent the container from properly rendering the application data to the user

These limitations make it so that the Docker environment is best suited for use in a non-interactive CI/CD pipeline
to perform a subset of the tests that you would otherwise be required to do manually.
Due to the fickleness of `pcscd` and `afrcdesktops.us.af.mil` and the interactive nature (by design) of cac authentication,
this Docker environment is only suitable for performing automated testing up to, (but not including),
the user's ability to authenticate using a valid cac to a functional/non-saturated `afrcdesktops.us.af.mil`.
Everything beyond that must be performed manually.

### Development and Testing with the Vagrant Environment

This repository does not include a Vagrant environment directly, due to the non-portability of Vagrant environments, in general; however,
it does includes instructions for building your environment below and an example file so that you are not starting from scratch.
You should only need to modify a couple of parameters that are specific to your host PC to get started.

Vagrant is a wrapper around `virtualbox`, `libvirt`, and other hypervisors that it calls 'providers'. It uses these providers to automate the setup and teardown of virutal machines.
These docs assume you are using the default `virtualbox` provider, but MRs that include instructions for others are welcome.

First, you'll need to follow the software and/or your distribution vendor's instructions for installing Vagrant.
Here is a link to the ones provided by Vagrant's vendor (Hashicorp): https://www.vagrantup.com/intro/getting-started/install.html

After it is installed, you should be able to run the following to test it:

```
$ vagrant
Usage: vagrant [options] <command> [<args>]

    -v, --version                    Print the version and exit.
    -h, --help                       Print this help.
```

Vagrant does not require plugins for using the default `virtualbox` provider, but other providers do require them.
You can see your list of currently installed plugins like so:

```
$ vagrant plugin list
vagrant-cachier (1.2.1, global)
vagrant-hostmanager (1.8.9, global)
vagrant-libvirt (0.0.45, global)
vagrant-scp (0.5.7, global)
```

This setup does not require the use of any plugins unless you decide to use a non-default provider.
If you wish to install any plugins, please refer to the Vagrant docs: https://www.vagrantup.com/docs/plugins/usage.html

In order to allow your `virtualbox` VMs to access ancillary devices like cac readers, you must install the extension pack.
This pack can either be installed directly from the software vendor (Oracle) by visiting their VirtualBox downloads page: https://www.virtualbox.org/wiki/Downloads,
or you can refer to your distribution vendor's instructions for performing this step.
As an example, for Ubuntu, you do the following:

```
$ sudo apt-get install virtualbox-ext-pack

# Output now shown for brevity
```

Next, you'll need to create a `Vagrantfile` in this repo's directory.
By default, Vagrant will mount this directory from the host into the VM at start time, so it is important that your Vagrantfile reside inside of this directory.
Typically a Vagrantfile is initialized with `vagrant init`, but this repo provides you with a less cluttered example file as a starting point.

```
cp Vagrantfile{.example,}

# No output if successful
```

This `Vagrantfile` includes a few parameters that you will need to change. Here is the example file's contents:

```
$ cat Vagrantfile.example
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"
  config.ssh.forward_x11 = true
  config.vm.network "public_network"
  config.vm.synced_folder "/etc/default/", "/etc/default/"
  config.vm.provision "shell", inline: <<-SHELL
     cd /vagrant;
     sudo chmod +x ./ubuntu.sh;
     sudo ./ubuntu.sh -y;
     sudo systemctl enable pcscd;
     sudo systemctl start pcscd;
  SHELL
  config.vm.provider :virtualbox do |vb|
      vb.customize ['modifyvm', :id, '--usb', 'on']
      vb.customize ['usbfilter', 'add', '0', '--target', :id, '--name', 'SCR33xx v2.0 USB SC Reader', '--vendorid', '0x04e6', '--productid', '0x5116']
  end
end
```

Some things of note about the file's contents:

- X11 forwarding is required in order to see `vmware-view` on the host when it runs
- Files located in the repo directory get mounted in `/vagrant` inside the VMs; changes in the VM flow to the host
- Toggling on the `--usb` parameter only works if you installed the VirtualBox Extension Pack
- Since `public_network` is specified, the VM connects via a bridge and at VM launch time will prompt you for the interface to use
- The values for `--vendorid` and `--productid` are unique to your cac reader and must be updated

Before you launch the VM, you'll need to modify (at minimum) the `--vendorid` and `--productid` values like so:

With root and/or a sudo user run the following:

```
$ sudo vboxmanage list usbhost
Host USB Devices:

# Other USB devices removed from output for brevity

UUID:               47fc47d0-e170-4703-acc8-76bf2eb1aca1
VendorId:           0x04e6 (04E6)
ProductId:          0x5116 (5116)
Revision:           6.1 (0601)
Port:               3
USB version/speed:  2/Full
Manufacturer:       Identive
Product:            SCR33xx v2.0 USB SC Reader
SerialNumber:       53311541700846
Address:            sysfs:/sys/devices/pci0000:00/0000:00:14.0/usb1/1-4//device:/dev/vboxusb/001/012
Current State:      Busy

```

Use the `VendorId` code (i.e. 0x04e6) and `ProductId` code (i.e. 0x5116) listed here in your `Vagrantfile`; `--name` is arbitrary, but feel free to update that as well.

Once your `Vagrantfile` is updated, make sure that `pcscd` (if installed) is temporarily turned off on the host machine so that the VM can mount it:

```
$ sudo systemctl stop pcscd

# Output not shown
```

Now you are ready to actually launch the VM.
Note: if this is the first time you have run `vagrant` it will need to pull down the ISO/vbox image that your VM will boot from.
This can take a while and will eat up some bandwidth, so ensure you are connected to a network with sufficient bandwidth before proceeding:

```
$ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'ubuntu/bionic64'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'ubuntu/bionic64' version '20200323.0.0' is up to date...
==> default: A newer version of the box 'ubuntu/bionic64' for provider 'virtualbox' is
==> default: available! You currently have version '20200323.0.0'. The latest is version
==> default: '20200325.0.0'. Run `vagrant box update` to update.
==> default: Setting the name of the VM: desktop-anywhere_default_1585323817572_40757
==> default: Clearing any previously set network interfaces...
==> default: Available bridged network interfaces:
1) wlp58s0
2) br-8ad51e4dc33f
3) br-f631115bfd58
4) br-980104a47601
5) enp57s0f1
6) br-bfc24544c38a
7) br-dc5d7b909dd8
8) br-df01e4b654b1
9) docker0
10) br-a7471cee2498
==> default: When choosing an interface, it is usually the one that is
==> default: being used to connect to the internet.
    default: Which interface should the network bridge to? 1
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
    default: Adapter 2: bridged
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default: Warning: Connection reset. Retrying...
    default: Warning: Remote connection disconnect. Retrying...
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
==> default: Configuring and enabling network interfaces...
==> default: Mounting shared folders...
    default: /vagrant => /yourhomedir/projects/desktop-anywhere
    default: /etc/default => /etc/default
==> default: Running provisioner: shell...
    default: Running: inline script

# Inline script output removed for brevity

```

If you look at the output above, you'll see the prompt for the bridge interface.
In this example, I provided "1" when prompted because that is the WiFi card for my laptop, yours may be different.
The list of available interfaces is listed slightly above the prompt, and you must provide the number, not the name.
If you want to set a default interface to use so that you are not being prompted at each `vagrant up`, 
please refer to Vagrant's documentation for setting this in the `Vagrantfile`: https://www.vagrantup.com/docs/networking/public_network.html#default-network-interface

You have probably noticed by now that this takes a while to bootstrap the VM.
If you are doing a lot of changes to the installer and trying get a faster feedback loop,
I recommend using the docker environment until you are pretty happy with your work and want to do some final end-to-end testing with in a full VM with Vagrant.

Once the VM is finished running the inline script block in the `Vagrantfile`, you will be able to login via this command:

```
$ vagrant ssh

# Output not shown
```

Note: If the inline script failed for some reason, run: `vagrant destroy`, make the needed fixes and try your `vagrant up` command again until it works.

Once you have sshed into the machine, this will drop you into a shell for the `vagrant` user on the system.
Changing directories to `/vagrant` will put you into the repo's directory mounted from the host machine:

```
$ cd /vagrant

# Output not shown
```

From here, you can run several tests, but most importantly, you'll want to see if you can actually launch `vmware-view`:

```
$ vmware-view

# Output not shown
```

*Warning: `vmware-view` does have `-q` and `-s` options, that is supposed to autoconnect (-q) you to the specified server (-s), but it breaks cac authentication!*
 
If this works, you should see the GUI for `vmware-view` pop up on your host machine.
Click on "Add Server" and enter the Desktop Anywhere URL (`afrcdesktops.us.af.mil).
Login to this server and click "Accept" when presented with the user agreement prompt.
Shortly afterwards, you should see a screen with cac cert selection options and a pin prompt.
From here, select your non-email DoD cac certificate and enter your pin to login.

If this works, you should see the options on Desktop Anywhere (i.e. full SDC, various software-as-a-service applications, etc).
Great success.

#### Troubleshooting Failures within the Vagrant Environment

If that did not work though, there could be a number of reasons why it did not:

- The cac reader middleware can not access the cac certs; run `pcsc_scan` to see if they are accessible; make sure `vmware-view` is not being executed with `-q` or `-s` as well
  - If at any point you accidentally ran `vmware-view` just once, just to see what it felt like,
    you may have to clean up your mess by removing any references to the server you passed to the `-s` flag in all the configs located in `~/.vmware/`
- The `pcscd` middleware starts, but cannot access the card reader; run `pcsc_scan` to see if it sees the reader; if not, check `lsusb` to see if the OS sees it
- The Desktop Anywhere service is just being fickle, because reasons; sometimes this happens and there is not much you can do about it except run `pcscd` in debug mode (-d flag) to rule it out as the issue
- If none of these seem to be the issue, check out the General Debugging section for some debugging tips

Other issues either have not been encountered and/or documented yet, or happen prior to an attempt to run `vmware-view`; MRs welcome.

### General Debugging

If you are getting issues that you suspect are with a specific component, there are things you can do to isolate and debug the issue further:

- If you suspect the script logic to be the issue, try setting `set -x` at the top of the file and following the logic to find logic errors
- If you suspect the script's syntax to be the issue, try running `shellcheck` against it to find these types of errors
- If you suspect the `entrypoint.sh` logic and/or syntax to be the issue, use the same `set -x` and/or `shellcheck` methods you used for the installer
- If you suspect the `Vagrantfile` inline scripting block to be the issue, strip out the script block, login to the vanilla machine, and run the steps manually to debug
- If you suspect the `Dockerfile` inline scripting to be the issue, spin up a vanilla base image, and run the steps manually to debug
- If you suspect `pcscd` to be the issue, trying running it with foreground and debug modes enabled `pcscd -fd`; if using `systemd` you can edit the unit file to do this for you
  - If you decide to modify the unit file, do so by editing `/lib/systemd/system/pcscd.service` directly or overriding it with `sudo systemctl edit pcscd.service`
  - Do not forget to run a `sudo systemctl daemon-reload` before restarting `pcscd.service` with `sudo systemctl restart pcscd.service`;
  - Use `sudo journalctl -fu pcscd.service` to follow the logs while services (i.e. `vmware-view`)  use `pcscd` to interact with the cac reader
- If you suspect `vmware-view` is the issue, turn on debugging in `~/.vmware/view-preferences` by adding the `view.defaultLogLevel = "0"` setting;
  that should give you some useful debug messages that you can search for in VMWare's docs or forums
  - Note, logs for each `vmware-view` execution dump to `/tmp/vmware-$USER/vmware-horizon-client-$pid.log` for each run
  - Since the `vmware-view` logs show different (more useful) output than the regular STDOUT seen on the screen when launching `vmware-view` from the terminal,
    it is sometimes helpful to immediately tail the log file upon execution.
    Doing this manually can be a bit tricky since the log is not created until the execution starts, and the $PID is part of the log's name.
    You can use this script to automate the starting and debugging of `vmware-view`, if it is giving you issues:

    ```
    #!/usr/bin/env bash
    
    /usr/bin/vmware-view &>/dev/null &
    until [[ "${pid:=0}" != "0" ]]; do
        sleep .1;
        pid="$(pgrep -f /usr/lib/vmware/view/bin/vmware-view)"
    done;
    log="/tmp/vmware-$USER/vmware-horizon-client-$pid.log";
    until [[ -e $log ]]; do
        sleep .1;
    done;
    while read -r line; do
      echo "${line}";
    done < <(tail -f "$log");
    ```

