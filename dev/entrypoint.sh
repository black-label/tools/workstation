#!/usr/bin/env bash

# @name entrypoint.sh
# @brief Entrypoint for the development environment container image.
# @description
#     This script executes some quality assurance on the user-facing code in ./src,
#     like the workstation.sh script. It runs things like an autoformatter,
#     linter, and an autodocs generator.
#     This ensures standards mentioned in the contributing guidelines,
#     (CONTRIBUTING.MD), are easier for developers to adhere to.

# @description
#    This function autoformats, lints, and generates docs for the workstation.sh script.
# @exitcode 0 If autoformatting, linting, and autodoc generation all pass.
# @exitcode 4 Failed to remove the previous autodoc file.
# @exitcode 5 If autoformatting, linting, or autodoc generation fail.
# @exitcode 6 If the location of the script is not a file.
# @see https://github.com/reconquest/shdoc#features
# @see https://github.com/koalaman/shellcheck#how-to-use
# @see https://github.com/mvdan/sh
epSelfHeal() {
  local autodoc_path
  local file
  local user
  user="$(id -un)"
  autodoc_path="/home/${user}/workstation/src/docs/workstation.sh.md"
  file="/home/${user}/workstation/src/workstation.sh"
  rm -f "${autodoc_path}" || return 4
  if [[ -f "${file}" ]]; then
    {
      echo -e "\n[INFO] ${FUNCNAME[0]} | Executing shfmt." &&
        shfmt -i 2 -sr -w "${file}" &&
        echo -e "\n[INFO] ${FUNCNAME[0]} | Executing shellcheck." &&
        shellcheck "${file}" &&
        echo -e "\n[INFO] ${FUNCNAME[0]} | Executing shdoc." &&
        shdoc "${file}" > "${autodoc_path}"
    } ||
      {
        echo -e "\n[WARN] The script (${file}) failed autoformatting, linting, and/or autodocs generation." 1>&2
        return 5
      }
  else
    echo -e "\n[FAIL] Location of this script (${file}) not a file." 1>&2
    return 6
  fi
}

# @description
#     This barebones function just calls the self-heal function for now.
#     Created this additional layer for consistency when add things in the future.
main() {
  epSelfHeal || {
    rc="${?}"
    return "${rc}"
  }
}

# @internal
#     This calls main and exits with its rc.
main "$@" || {
  rc="${?}"
  exit "$rc"
}
