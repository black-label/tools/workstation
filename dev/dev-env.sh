#!/usr/bin/env bash

# @name dev-env.sh
# @brief Manages a local dev and test environment
# @description
#     This script builds and launches a local development environment.
#     It also runs locally executed test cases and builds matching pipeline jobs.

# @description
#     This function gets a top-level directory that can be used by other functions.
#     This allows those functions to specify absolute paths instead of relative ones.
#     This ensures functions still work, regardless of where script is executes from.
wsdevGetTld() {
  # Ensures pwd and dirname bins exist before trying to use them to parse and cd to script path
  # Test fails if which is passed a bash builtin (i.e. cd), or returns multiple paths, or which not in path
  if [[ -f "$(which pwd)" ]] && [[ -f "$(which dirname)" ]]; then
    # No _?_* prefix naming convention indicates this is a locally scoped variable
     local wsdev_script_dir
     wsdev_script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
     cd "${wsdev_script_dir}" || return 1
     echo -e "\n[INFO] ${FUNCNAME[0]} | This script (${BASH_SOURCE[0]}) is located in the following directory:"
     echo -e   "       ${wsdev_script_dir}"
     # We know that the script is in ./dev, so cd up to get one dir to get the path of the dir above that
     cd ../ || return 1
     local wsdev_tld_dir
     wsdev_tld_dir="$( cd "$( dirname "${wsdev_script_dir}" )" && pwd )"
  else
     echo -e "\n[FAIL] ${FUNCNAME[0]} | Required dependencies (pwd and/or dirname) not present in path." 1>&2
     return 2
  fi
  if [[ -d "${wsdev_tld_dir}" ]]; then
    # Exporting var to make available to subshells
    # Setting readonly/constant since var won't change after this
    # _ec_* naming convention indicates readonly/constant variable types
    readonly _ec_wsdev_tld_dir="$wsdev_tld_dir"
    export _ec_wsdev_tld_dir
    echo -e "\n[INFO] ${FUNCNAME[0]} | The top-level directory is located at the following path:"
    echo -e   "       ${_ec_wsdev_tld_dir}"
    cd "${_ec_wsdev_tld_dir}" || return 1
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Parsed script directory path not a valid directory." 1>&2
    return 1
  fi
}

# @description
#     This function builds the development environment docker container image.
#     The image contains utilities for ensuring code quality.
#     THe image is used by other functions to launch a container in different ways.
wsdevBuildDevEnv() {
  cd "${_ec_wsdev_tld_dir}" || return 1
  TLD="$PWD" \
  LOCAL_USER="$(id -un)" \
  docker compose \
    -f "./dev/docker-compose.yml" \
    build \
    --build-arg LOCAL_USER="$(id -un)" \
    --build-arg LOCAL_UID="$(id -u)" \
    --build-arg LOCAL_GROUP="$(id -gn)" \
    --build-arg LOCAL_GID="$(id -g)" \
    --no-cache \
    workstation_sh_dev_env ||
  {
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Failed to build workstation_sh_dev environment container.\n" 1>&2
    return 1
  }
}

# @description
#     This function launches a development env container interactive.
#     The container has development utilities pre-installed.
wsdevLaunchDevEnvShell() {
  cd "${_ec_wsdev_tld_dir}" || return 1
  TLD="$PWD" \
  LOCAL_USER="$(id -un)" \
  docker compose \
    -f "./dev/docker-compose.yml" \
    run \
    --rm \
    workstation_sh_dev_env_shell ||
  {
    echo -e "\n[FAIL] ${FUNCNAME[0]} | workstation_sh_dev_env_shell failed.\n" 1>&2
    return 1
  }
}

# @description
#     This function launches a development env container non-interactively.
#     The entrypoint.sh script executes some development utilities that do things like:
#     lint, autoformat, and generate documentation for files in ./src like workstation.sh
wsdevLaunchDevEnvEntrypoint() {
  cd "${_ec_wsdev_tld_dir}" || return 1
  TLD="$PWD" \
  LOCAL_USER="$(id -un)" \
  docker compose \
    -f "./dev/docker-compose.yml" \
    run \
    --rm \
    workstation_sh_dev_env_entrypoint ||
  {
    echo -e "\n[FAIL] ${FUNCNAME[0]} | workstation_sh_dev_env_entrypoint failed.\n" 1>&2
    return 1
  }
}

# @description
#     This function launches a single test case.
#     If the test case runs and returns the expected rc, it passes.
#     If the test case runs and returns an unexpected rc, it fails.
#     See the CONTRIBUTING.MD file for latest guidance on how to setup test cases.
wsdevRunTestCase() {
  local test_case
  local expected_rc
  local actual_rc
  local -a script
  local container
  test_case="${1}"
  if [[ ! -d "${_ec_wsdev_tld_dir}/test/cases/${test_case}/" ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Test case (${test_case}) directory not found." 1>&2
    return 1
  fi
  if [[ -f "${_ec_wsdev_tld_dir}/test/cases/${test_case}/script" ]]; then
    while read -r word; do
      script+=( "${word}" )
    done < "${_ec_wsdev_tld_dir}/test/cases/${test_case}/script"
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Test case (${test_case}) script file not found." 1>&2
    return 1
  fi
  if [[ "${#script[@]}" == "0" ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | No command found in script file for test case (${test_case})." 1>&2
    return 1
  fi
  if [[ -f "${_ec_wsdev_tld_dir}/test/cases/${test_case}/before_script" ]]; then
    read -r before_script < "${_ec_wsdev_tld_dir}/test/cases/${test_case}/before_script"
  else
    echo -e "\n[WARN] ${FUNCNAME[0]} | Test case (${test_case}) before_script file not found." 1>&2
  fi
  if [[ "${#before_script}" == "0" ]]; then
    echo -e "\n[WARN] ${FUNCNAME[0]} | No command found in before_script file for test case (${test_case})." 1>&2
  fi
  if [[ -f "${_ec_wsdev_tld_dir}/test/cases/${test_case}/rc" ]]; then
    read -r expected_rc < "${_ec_wsdev_tld_dir}/test/cases/${test_case}/rc"
  else
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Test case (${test_case}) expected rc not found." 1>&2
    return 1
  fi
  if [[ "${#expected_rc}" == "0" ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | No return code found in rc file for test case (${test_case})." 1>&2
    return 1
  fi
  # Expected return code should contain numbers only, and at least one character
  if [[ ! "${expected_rc}" =~ ^[0-9]+$ ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Failed to parse a valid expected return code from rc file for test case (${test_case})." 1>&2
    return 1
  fi
  if [[ -f "${_ec_wsdev_tld_dir}/test/cases/${test_case}/container" ]]; then
    read -r container < "${_ec_wsdev_tld_dir}/test/cases/${test_case}/container"
    if [[ "${#container}" == "0" ]]; then
      echo -e "\n[WARN] ${FUNCNAME[0]} | No container image name found in container file for test case (${test_case})." 1>&2
    fi
    echo -e "\n[INFO] ${FUNCNAME[0]} | Running test case (${test_case})."
    echo FROM "${container} as ${test_case}_base" > "${_ec_wsdev_tld_dir}/test/cases/${test_case}/Dockerfile"
    echo RUN "${before_script}" >> "${_ec_wsdev_tld_dir}/test/cases/${test_case}/Dockerfile"
    docker build \
      --file "${_ec_wsdev_tld_dir}/test/cases/${test_case}/Dockerfile" \
      --target "${test_case}_base" \
      --tag "wsdev_${test_case}_base" \
      "${_ec_wsdev_tld_dir}" ||
        {
          echo -e "\n[FAIL] Failed to build docker image for test case ($test_case)."
          return 1
        }
    docker run \
      --rm \
      -v "${_ec_wsdev_tld_dir}:/workdir" \
      -w /workdir \
      "wsdev_${test_case}_base" \
      /usr/bin/env sh -c "/usr/bin/env bash ${script[*]}"
    actual_rc="$?"
  else
    echo -e "\n[WARN] ${FUNCNAME[0]} | No container image name passed for test case." 1>&2
  fi
  if [[ "$actual_rc" != "$expected_rc" ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | Actual return code (${actual_rc}) did not match expected return code (${expected_rc})." 1>&2
    return 1
  else
    echo -e "\n[PASS] ${FUNCNAME[0]} | Actual return code (${actual_rc}) matched expected return code (${expected_rc}) for test case (${test_case})."
  fi
  # Create a .gitlab-ci.yml if test passed locally and had a 'container' file
  if [[ "${#container}" != "0" ]]; then
     echo > "${_ec_wsdev_tld_dir}/test/cases/${test_case}/.gitlab-ci.yml"
     {
       echo "---"
       echo "${test_case}:"
       echo "  stage: test"
       echo "  image: ${container}"
       echo "  variables:"
       echo "    expected_rc: ${expected_rc}"
       echo "  before_script:"
       echo "    - ${before_script:=true}"
       echo "  script:"
       echo "    - ./test/run-test.sh ${expected_rc} ${script[*]}"
       echo "  only:"
       echo "    - merge_requests"
       echo "..."
     } >> "${_ec_wsdev_tld_dir}/test/cases/${test_case}/.gitlab-ci.yml"
  fi
}

# @description
#     This function loops through ./test/cases/* and runs each test case in it.
#     It calls wsdevRunTestCase for each test case directory it finds.
#     Even if a single test fails, it continues to loop.
#     It reports test case failures (if any) at the end and returns appropriately.
wsdevRunAllTests() {
  local test_dir
  local test_case_name
  local -a test_failures
  local test_failure
  test_failures=( )
  echo -e "\n[INFO] ${FUNCNAME[0]} | Running test cases in: ${_ec_wsdev_tld_dir}/test/cases/"
  for test_dir in "${_ec_wsdev_tld_dir}"/test/cases/*; do
    # Strip everything before (and including) the last slash from test_dir
    test_case_name="${test_dir##*/}"
    if [[ -d "${test_dir}" ]]; then
      wsdevRunTestCase "${test_case_name}" || {
        test_failures+=( "${test_case_name}" )
      }
    else
      echo -e "\n[WARN] ${FUNCNAME[0]} | Item in test cases directory (${_ec_wsdev_tld_dir}/test/cases/) not a directory." 1>&2 
    fi
  done
  if [[ "${#test_failures[@]}" != "0" ]]; then
    echo -e "\n[FAIL] ${FUNCNAME[0]} | The follow test cases failed:" 1>&2
    for test_failure in "${test_failures[@]}"; do
      echo "       - ${test_failure}" 1>&2
    done
    return 1
  else
    echo -e "\n[PASS] ${FUNCNAME[0]} | All test cases passed! :-)\n"
  fi
}

# @description
#     This is the main/entrypoint function.
#     It contains a list of all the subcommands available in the script.
#     Very little logic lives in the main function,
#      mostly just calls to other funcs and handling of return codes from them.
main() {
  wsdevGetTld || { rc="${?}"; return "$rc"; }
  case "$1" in
    build)
      wsdevBuildDevEnv || { rc="${?}"; return "$rc"; }
      ;;
    launch-shell)
      wsdevLaunchDevEnvShell || { rc="${?}"; return "$rc"; }
      ;;
    launch-entrypoint)
      wsdevLaunchDevEnvEntrypoint || { rc="${?}"; return "$rc"; }
      ;;
    run-test)
      test_case_name="${2}"
      wsdevRunTestCase "${test_case_name}" || { rc="${?}"; return "$rc"; }
      ;;
    run-all-tests)
      wsdevRunAllTests || { rc="$?"; return "$rc"; }
      ;;
    *)
      { wsdevBuildDevEnv && wsdevLaunchDevEnvEntrypoint; } || { rc="${?}"; return "$rc"; }
      ;;
  esac
}

# @internal
#    This is the call to the main function.
#    It uses the return code from the main function to exit with the same rc.
main "$@" || { rc="${?}"; exit "$rc"; }
