FROM registry.gitlab.com/dreamer-labs/tsc/pipelines/bashutils-final:v2-mr31 as workstation_sh_dev_env_base
RUN apk --no-cache add shadow coreutils musl-utils

FROM workstation_sh_dev_env_base as workstation_sh_dev_env_final
ARG TLD
ARG LOCAL_USER
ARG LOCAL_UID
ARG LOCAL_GROUP
ARG LOCAL_GID
WORKDIR /home/$LOCAL_USER/workstation
COPY ./dev/entrypoint.sh /home/$LOCAL_USER/entrypoint.sh
RUN ( getent group "${LOCAL_GROUP}" || groupadd --gid "${LOCAL_GID}" "${LOCAL_GROUP}" ) &&                    \
    useradd -m --non-unique --uid "${LOCAL_UID}" --gid "${LOCAL_GID}" --shell /usr/local/bin/bash "${LOCAL_USER}" &&    \
    echo 'export PS1="(workstation.sh dev) [\u@workstation \W]\$ "' >> "/home/$LOCAL_USER/.bashrc" &&   \
    mkdir -p /home/${LOCAL_USER}/workstation/ &&                                                          \
    chown -R "${LOCAL_USER}":"${LOCAL_GROUP}" "/home/${LOCAL_USER}/" && \
    chmod +x "/home/${LOCAL_USER}/entrypoint.sh"
USER "$LOCAL_USER"
